import { addValueArray }from './fn/addValueArray'
import { addAttrConstructor }from './fn/addAttrConstructor'


addValueArray(
    `
    { path: '**', redirectTo: 'teste' }`,
    'routes',
    'routes'
);

addValueArray(
    `  
    {
        text: 'Planejamento',
        path: 'planejamento',
        icon: 'event',
        data: {
            permissions: {
            only: 'PLANEJADOR',
            },
        },
    }
    
    `,
    'navigation',
    'navigation'
);

addAttrConstructor(["dialog","Dialog"]);