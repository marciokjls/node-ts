import {Project,SyntaxKind} from "ts-morph";
import * as fs from 'fs';


export function addValueArray(value: string, archive:string, array: string){

    const project = new Project();
    const Path = `src/app-${archive}.ts`;
    const fileContent = fs.readFileSync(Path, 'utf-8');
    const sourceFile = project.createSourceFile(Path, fileContent, { overwrite: true });
    const arrayVariable = sourceFile.getVariableDeclaration(array);

    if (arrayVariable) {
        const arrayLiteral = arrayVariable.getInitializerIfKind(SyntaxKind.ArrayLiteralExpression);

        if (arrayLiteral) {
            const elements = arrayLiteral.getElements();
                if (elements.length > 0) {
                    arrayLiteral.insertElement(elements.length, value);
                } else {
                    arrayLiteral.addElement(value);
                }
        }

    }
    console.log(`Atualizando o app-${archive}.ts`)
    const updatedContent = sourceFile.getFullText();
    fs.writeFileSync(Path,updatedContent)
}