import { Project, SourceFile, SyntaxKind } from "ts-morph";
import * as fs from 'fs';


export function addAttrConstructor(value: string[]){

  const project = new Project();
  const Path = `src/app.component.ts`;
  const fileContent = fs.readFileSync(Path, 'utf-8');
  const sourceFile = project.createSourceFile(Path, fileContent, { overwrite: true });
  const classDeclaration = sourceFile.getClasses()[0];

  const constructor = classDeclaration.getConstructors()[0];


  // add os parametros no construtor
  const newParameter = constructor.addParameter({
    name: value[0],
    type: value[1],
  });

  // Definindo parametro como privado
  newParameter.toggleModifier("private");


  //inicialização dos attr
  // constructor.setBodyText(writer => {
  //   writer.writeLine(`this.parametroConstrutor = [parametroConstrutor];`);
  // });

  console.log(`Atualizando o app.component.ts`)
  const updatedContent = sourceFile.getFullText();
  fs.writeFileSync(Path,updatedContent)
}